#!/usr/bin/env python3

import cgi
import cgitb
import psycopg2
import sys
import codecs

from psycopg2 import sql
'''
from user import User

cgitb.enable()

sys.stdout = codecs.getwriter("utf-8")(sys.stdout.detach())

auth = User.generate_user()
new_user = auth[0]
password = auth[1]

usnm = new_user.get_username()
pswd = new_user.get_password()

conn = psycopg2.connect(
    dbname='postgres',
    user='myuser',
    password='UigfjL0f',
    host='localhost'
)
with conn.cursor() as cursor:
    conn.autocommit = True
    values = [
        (0, usnm, pswd)
    ]
    insert = sql.SQL(
        'INSERT INTO users (id_user, username, password) VALUES {}').format(
        sql.SQL(',').join(map(sql.Literal, values))
    )
    cursor.execute(insert)
conn.close()
'''
print("Content-type: text/html")
print('''<DOCTYPE html>
    <html lang="ru">
    <head>
        <title>backend-4</title>
        <meta charset="UTF-8">
        <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.0-beta1/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-giJF6kkoqNQ00vy+HMDP7azOuL0xtbfIcaT9wjKHr8RbDVddVHyTfAAsrekwKmP1" crossorigin="anonymous">
    </head>
    <body>
        <h1>Ваш логин: {usnm}</h1>
        <h1>Ваш пароль: {password}</h1>
        <a href="login.html" class="btn btn-primary">Авторизоваться</a>
    </body>
    </html>''')
