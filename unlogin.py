#!/usr/bin/env python3

print('Set-Cookie: session_id=None; expires=Thu Jan 01 1970 00:00:00')
print('Set-Cookie: username=None; expires=Thu Jan 01 1970 00:00:00')
print("Content-type: text/html")
print(f'''
    <DOCTYPE html>
    <html lang="ru">
        <head>
            <title>backend-3</title>
            <meta charset="UTF-8">
            <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.0-beta1/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-giJF6kkoqNQ00vy+HMDP7azOuL0xtbfIcaT9wjKHr8RbDVddVHyTfAAsrekwKmP1" crossorigin="anonymous">
        </head>
        <body>
            <a href="/backend-5/registration.html" class="btn btn-primary">Регистрация</a>
            <a href="/backend-5/login.html" class="btn btn-success">Авторизация</a>
            <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.0.0-beta1/dist/js/bootstrap.bundle.min.js" integrity="sha384-ygbV9kiqUc6oa4msXn9868pTtWMgiQaeYH7/t7LECLbyPA2x65Kgf80OJFdroafW" crossorigin="anonymous"></script>
        </body>
    </html>
    ''')